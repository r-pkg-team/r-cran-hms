r-cran-hms (1.1.3-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.2 (routine-update)
  * dh-update-R to update Build-Depends (routine-update)

 -- Andreas Tille <tille@debian.org>  Thu, 22 Jun 2023 16:55:09 +0200

r-cran-hms (1.1.2-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.1 (routine-update)
  * Reorder sequence of d/control fields by cme (routine-update)

 -- Andreas Tille <tille@debian.org>  Mon, 22 Aug 2022 08:48:05 +0200

r-cran-hms (1.1.1-1) unstable; urgency=medium

  * Team Upload.
  [ Andreas Tille ]
  * Disable reprotest
  * Disable blhc

  [ Nilesh Patra ]
  * New upstream version 1.1.1

 -- Nilesh Patra <nilesh@debian.org>  Sat, 09 Oct 2021 14:50:25 +0000

r-cran-hms (1.1.0-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.0 (routine-update)
  * dh-update-R to update Build-Depends (routine-update)

 -- Andreas Tille <tille@debian.org>  Mon, 20 Sep 2021 12:42:17 +0200

r-cran-hms (1.0.0-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.5.1 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Testsuite: autopkgtest-pkg-r (routine-update)

 -- Andreas Tille <tille@debian.org>  Fri, 15 Jan 2021 23:34:35 +0100

r-cran-hms (0.5.3-2) unstable; urgency=medium

  * Standards-Version: 4.5.0 (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)

 -- Andreas Tille <tille@debian.org>  Sun, 17 May 2020 21:38:17 +0200

r-cran-hms (0.5.3-1) unstable; urgency=medium

  * New upstream version
  * dh-update-R to update Build-Depends
  * Set upstream metadata fields: Bug-Submit, Repository.

 -- Andreas Tille <tille@debian.org>  Sun, 12 Jan 2020 22:13:53 +0100

r-cran-hms (0.5.2-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.4.1
  * autopkgtest: s/ADTTMP/AUTOPKGTEST_TMP/g

 -- Andreas Tille <tille@debian.org>  Tue, 12 Nov 2019 11:47:42 +0100

r-cran-hms (0.5.1-1) unstable; urgency=medium

  * New upstream version
  * debhelper-compat 12
  * dh-update-R to update Build-Depends
  * Set upstream metadata fields: Archive, Bug-Database, Repository.

 -- Andreas Tille <tille@debian.org>  Mon, 26 Aug 2019 10:49:19 +0200

r-cran-hms (0.5.0-1) unstable; urgency=medium

  * New upstream version
  * debhelper 12
  * Standards-Version: 4.4.0
  * Build-Depends: r-cran-vctrs

 -- Andreas Tille <tille@debian.org>  Fri, 19 Jul 2019 10:28:19 +0200

r-cran-hms (0.4.2-2) unstable; urgency=medium

  * Rebuild for R 3.5 transition
  * Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-
    lists.debian.net>
  * Point Vcs fields to salsa.debian.org
  * dh-update-R to update Build-Depends

 -- Andreas Tille <tille@debian.org>  Fri, 01 Jun 2018 08:01:38 +0200

r-cran-hms (0.4.2-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Wed, 14 Mar 2018 18:50:32 +0100

r-cran-hms (0.4.0-2) unstable; urgency=medium

  * debian/tests/control: Depends: r-cran-pillar
    Closes: #886284
  * debhelper 11

 -- Andreas Tille <tille@debian.org>  Tue, 16 Jan 2018 14:18:50 +0100

r-cran-hms (0.4.0-1) unstable; urgency=medium

  * New upstream version
    Closes: #886089
  * New Build-Depends: r-cran-pkgconfig, r-cran-rlang
  * Standards-Version: 4.1.3

 -- Andreas Tille <tille@debian.org>  Tue, 02 Jan 2018 08:13:23 +0100

r-cran-hms (0.3-1) unstable; urgency=medium

  * Initial release (closes: #877479)

 -- Andreas Tille <tille@debian.org>  Mon, 02 Oct 2017 08:33:54 +0200
